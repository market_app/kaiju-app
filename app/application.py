"""
Web app initialization.
"""

from pathlib import Path

import sqlalchemy as sa
from aiohttp.web import Application

from kaiju_tools.logging import init_logger, get_logger_settings
from kaiju_tools.http.exceptions import error_middleware
from kaiju_tools.services import ServiceContextManager
from kaiju_tools.rpc.services import JSONRPCServer
from kaiju_db.services import DatabaseService

from app.models import *
from app.routes import urls
from app.service_registry import service_class_registry

__all__ = ('init_app', 'WebApplication')

DIR = Path(__file__).parent


class WebAppServices(ServiceContextManager):
    """For type hinting"""

    db: DatabaseService
    rpc: JSONRPCServer


class WebApplication(Application):
    """For type hinting"""

    settings: object
    name: str
    id: str
    db_meta: sa.MetaData

    # services

    services: WebAppServices
    db: DatabaseService


def init_app(settings) -> WebApplication:
    """Returns a web app ready to run."""

    logger_settings = get_logger_settings(settings.main['loglevel'])
    logger = init_logger(logger_settings)
    app = Application(middlewares=[error_middleware], logger=logger, **settings.app)

    app.name = settings.main['name']
    app.id = settings.main['id']
    app.settings = settings
    app.db_meta = metadata

    services = ServiceContextManager(
        app=app, settings=settings.services,
        class_registry=service_class_registry,
        logger=app.logger
    )
    app.cleanup_ctx.extend(services)

    app.router.add_static('/static/', path=str(DIR.parent / 'static'))

    for method, reg, handler, name in urls:
        app.router.add_route(method, reg, handler)
        app.router.add_route(method, reg + "/", handler, name=name)

    return app
