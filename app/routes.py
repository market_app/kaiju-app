"""
Write your routes here.

`urls` - url list which will be registered in the app's router
"""

from app.views import *

urls = [
    ('*', r'/rpc', JSONRPCView, 'json_rpc_view'),
    ('*', r'/rpc/{method}', JSONRPCMethodView, 'json_rpc_method_view'),
]
