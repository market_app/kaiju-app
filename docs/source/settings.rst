APP.SETTINGS
============

Contains various app settings.

services
--------

.. automodule:: app.settings.services
   :members:
   :undoc-members:
   :show-inheritance:

base
----

.. automodule:: app.settings.base
   :members:
   :undoc-members:
   :show-inheritance:

local
-----

.. automodule:: app.settings.local
   :members:
   :undoc-members:
   :show-inheritance:

CLI
---

.. automodule:: app.settings.CLI
   :members:
   :undoc-members:
   :show-inheritance:

CLI_local
---------

.. automodule:: app.settings.CLI_local
   :members:
   :undoc-members:
   :show-inheritance:
